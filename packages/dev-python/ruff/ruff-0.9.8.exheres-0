# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cargo [ rust_minimum_version=1.83.0 ]
require github [ user=astral-sh ]
require bash-completion zsh-completion
require py-pep517 [ backend=maturin backend_version_spec="[>=1.0&<2.0]" entrypoints=[ ${PN} ] ]
require flag-o-matic

SUMMARY="An extremely fast Python linter, written in Rust"
DESCRIPTION="
Ruff aims to be orders of magnitude faster than alternative tools while
integrating more functionality behind a single, common interface.
Ruff can be used to replace Flake8 (plus dozens of plugins), isort,
pydocstyle, yesqa, eradicate, pyupgrade, and autoflake, all while executing
tens or hundreds of times faster than any individual tool."

HOMEPAGE+=" https://beta.ruff.rs/"

UPSTREAM_CHANGELOG="https://github.com/astral-sh/ruff/blob/main/CHANGELOG.md"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
"

unpack_one_multibuild() {
    default

    edo pushd "${PYTHON_WORK}"
    ecargo_fetch
    edo popd
}

configure_one_multibuild() {
    # "undefined reference to `_rjem_sdallocx'"
    filter-flags -flto

    py-pep517_configure_one_multibuild
}

test_one_multibuild() {
    # Ironically these test pass locally, but fail on Gitlab's CI
    cargo_src_test -- \
        --skip display_default_settings \
        --skip message::gitlab::tests::output \
        --skip message::gitlab::tests::syntax_errors
}

install_one_multibuild() {
    py-pep517_install_one_multibuild

    if option bash-completion ; then
        edo target/release/ruff generate-shell-completion bash > ${PN}.bash
        dobashcompletion ${PN}.bash
    fi

    if option zsh-completion ; then
        edo target/release/ruff generate-shell-completion zsh > _${PN}
        dozshcompletion _${PN}
    fi
}

