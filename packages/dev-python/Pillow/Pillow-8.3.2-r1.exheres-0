# Copyright 2014 Calvin Walton <calvin.walton@kepstin.ca>
# Distributed under the terms of the GNU General Public License v2

SETUP_CFG_COMMANDS="build_ext"

require pypi setup-py [ import=setuptools blacklist=2 cfg_file=setup python_opts="[tk?]" test=nose ]

SUMMARY="Python Imaging Library (Fork)"
DESCRIPTION="
Pillow is the \"friendly\" PIL fork by Alex Clark and Contributors. PIL
is the Python Imaging Library by Fredrik Lundh and Contributors.
"
HOMEPAGE+=" https://python-pillow.org"

LICENCES="HPND"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~x86"
MYOPTIONS="
    jpeg2000
    lcms
    scanner
    tiff
    webp
    X
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        media-libs/freetype:2
        sys-libs/zlib
        jpeg2000? ( media-libs/OpenJPEG:2 )
        lcms? ( media-libs/lcms2 )
        providers:ijg-jpeg? ( media-libs/jpeg:=[>=6b] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        scanner? ( dev-python/python-sane[python_abis:*(-)?] )
        tiff? ( media-libs/tiff:= )
        webp? ( media-libs/libwebp:=[>=0.6.1] )
        X? ( x11-libs/libxcb )
    test:
        app-text/ghostscript
        dev-python/coverage[python_abis:*(-)?]
    suggestion:
        app-text/ghostscript [[ description = [ Enable EPS image plugin ] ]]
"

SETUP_CFG_build_ext_PARAMS=(
    "enable_freetype = True"
    "enable_jpeg = True"
    "enable_zlib = True"
    "disable_imagequant = True"
    "disable_raqm = True"
)

pillow_option_enable() {
    option "${1}" "enable_${2:-${1}} = True" "disable_${2:-${1}} = True"
}

setup-cfg_build_ext_append() {
    (
        pillow_option_enable jpeg2000
        pillow_option_enable lcms
        pillow_option_enable tiff
        pillow_option_enable webp
        pillow_option_enable webp webpmux
        pillow_option_enable X xcb
    ) >>setup.cfg
}

setup-py_test_one_multibuild() {
    PYTHONPATH=$(echo "${PWD}"/build/lib*) edo ${PYTHON} -B selftest.py --installed
    # TODO(somasis): nose tests currently fail due to an ImportError...
    #                ImportError: cannot import name '_imaging'
    #PYTHONPATH=$(echo "${PWD}"/build/lib*) edo nosetests -vx Tests/test_*.py
}

