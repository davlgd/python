# Copyright 2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi py-pep517 [ backend=self-hosted blacklist=3.8 test=pytest ]

SUMMARY="Accelerated property cache"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# missing test dependency: pytest-codspeed
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-python/build[python_abis:*(-)?]
        dev-python/Cython[python_abis:*(-)?]
        dev-python/expandvars[python_abis:*(-)?]
        dev-python/setuptools:0[>=47][python_abis:*(-)?]
        python_abis:3.9? ( dev-python/tomli[python_abis:3.9] )
        python_abis:3.10? ( dev-python/tomli[python_abis:3.10] )

"

compile_one_multibuild() {
    # This is a bit weird, the usual gpep517 invocation reports
    # "gpep517 INFO The backend produced dist/propcache-0.2.1-cp312-cp312-linux_x86_64.whl"
    # but this file doesn't exist.
    edo ${PYTHON} -m build --no-isolation --wheel
}

