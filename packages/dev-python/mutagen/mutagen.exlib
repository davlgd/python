# Copyright 2009 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=quodlibet tag=release-${PV} ]
require py-pep517 [ backend=setuptools work=${PN}-release-${PV} test=pytest entrypoints=[ mid3{cp,iconv,v2} moggsplit mutagen-{inspect,pony} ] ]
require utf8-locale

export_exlib_phases pkg_setup src_install

SUMMARY="Python module to handle audio metadata"
DESCRIPTION="
Mutagen supports ASF, FLAC, M4A, Monkey's Audio, MP3, Musepack, Ogg FLAC, Ogg
Speex, Ogg Theora, Ogg Vorbis, True Audio, WavPack and OptimFROG audio files.
All versions of ID3v2 are supported, and all standard ID3v2.4 frames are parsed.
It can read Xing headers to accurately calculate the bitrate and length of MP3s.
ID3 and APEv2 tags can be edited regardless of audio format. It can also
manipulate Ogg streams on an individual packet/page level.
"

REMOTE_IDS+=" pypi:${PN}"

UPSTREAM_CHANGELOG="https://${PN}.readthedocs.io/en/latest/changelog.html"
UPSTREAM_DOCUMENTATION="https://${PN}.readthedocs.io"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    test:
        dev-python/hypothesis[python_abis:*(-)?]
        media-libs/faad2
        media-libs/flac:*[ogg]
        media-sound/vorbis-tools
"

mutagen_pkg_setup() {
    # tests require utf8
    require_utf8_locale
}

install_one_multibuild() {
    py-pep517_install_one_multibuild

    # Remove man page in wrong location and install it into the correct one later
    edo rm -r "${IMAGE}"/usr/$(exhost --target)/share
}

mutagen_src_install() {
    py-pep517_src_install

    doman "${WORK}"/PYTHON_ABIS/$(python_get_abi)/"${PYTHON_WORK}"/man/*.1
}

