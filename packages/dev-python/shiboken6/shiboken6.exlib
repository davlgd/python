# Copyright 2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi python [ blacklist=2 multibuild=false ] cmake
require llvm [ llvm_slots=[ 19 18 ] additional_packages=[ "dev-lang/clang" ] ]

MY_PN="pyside-setup-everywhere-src"

export_exlib_phases src_prepare

SUMMARY="CPython-based binding code generator for C or C++ libraries"
DESCRIPTION="
It uses an ApiExtractor library to parse the C or C++ headers and get the type
information, using Clang. The library is used for PySide6 bindings but can
also be used to parse non-Qt projects."

HOMEPAGE+=" https://doc.qt.io/qtforpython/${PN}/"
DOWNLOADS="
    mirror://qt/official_releases/QtForPython/pyside6/PySide6-${PV}-src/${MY_PN}-${PV}.tar.xz
"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS=""

QT_VERSION="$(ever range 1-2)"

DEPENDENCIES="
    build+run:
        dev-libs/libxml2:2.0[>=2.6.32]
        dev-libs/libxslt[>=1.1.19]
        dev-python/numpy[python_abis:*(-)?]
        x11-libs/qtbase:6[~>${QT_VERSION}]
"

# A lot of tests fail and need hand holding
RESTRICT="test"

CMAKE_SOURCE="${WORKBASE}"/${MY_PN}-$(ever range -3 )/sources/${PN}

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DPython_EXECUTABLE=${PYTHON}
    -DClang_DIR=/usr/$(exhost --target)/lib/llvm/$(llvm_get_abi)/lib/cmake/clang
)
CMAKE_SRC_CONFIGURE_TESTS+=(
    '-DBUILD_TESTS:BOOL=TRUE -DBUILD_TESTS:BOOL=FALSE'
)

shiboken6_src_prepare() {
    cmake_src_prepare

    edo sed -e "s:share/man/man1:/usr/&:" -i doc/CMakeLists.txt
}

