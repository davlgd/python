# Copyright 2013 Jorge Aparicio
# Copyright 2021 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi
require setup-py [ import=setuptools test=pytest ]

SUMMARY="The modular source code checker: pycodestyle, pyflakes and McCabe"
HOMEPAGE+=" https://gitlab.com/pycqa/${PN}"

UPSTREAM_DOCUMENTATION="https://${PN}.pycqa.org"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/mccabe[>=0.6.0&<0.7.0][python_abis:*(-)?]
        dev-python/pycodestyle[>=2.7.0&<2.8.0][python_abis:*(-)?]
        dev-python/pyflakes[>=2.3.0&<2.4.0][python_abis:*(-)?]
        python_abis:2.7? (
            dev-python/configparser[python_abis:2.7]
            dev-python/enum34[python_abis:2.7]
            dev-python/functools32[python_abis:2.7]
            dev-python/typing[python_abis:2.7]
        )
    test:
        dev-python/mock[>=2.0.0][python_abis:*(-)?]
"

test_one_multibuild() {
    local abi_temp="${TEMP}${MULTIBUILD_TARGET}"
    local test_dir="${abi_temp}"/test
    local test_libdir="${test_dir}"/lib
    local test_log="${abi_temp}"/test.log

    # Remove broken tests on Python 2, last checked: 3.7.9
    if [[ $(python_get_abi) == 2.* ]]; then
        edo rm tests/integration/test_{aggregator,api_legacy,main,plugins}.py
        edo rm tests/unit/test_{config_file_finder,get_local_plugins,merged_config_parser,setuptools_command}.py
    fi

    # Install to register entry-points for built-in plugins
    edo mkdir -p "${test_libdir}"
    export PYTHONPATH="${test_libdir}"
    export PATH="${test_dir}/bin:${PATH}"
    edo ${PYTHON} -B setup.py install \
        --home="${test_dir}" \
        --install-lib="${test_libdir}"      \
        --single-version-externally-managed \
        --record="${abi_temp}"/record.log   \
        --no-compile
    edo ${PYTHON} -B -m pytest tests

    unset PYTHONPATH
}

