Source/Upsteam: Yes, fixed in git master
Reason: Allow pyparsing >= 2.3.1

From 7d070b892040976b25811164a8de78b77e861d04 Mon Sep 17 00:00:00 2001
From: Kyle Sunden <git@ksunden.space>
Date: Tue, 1 Aug 2023 11:24:38 -0500
Subject: [PATCH] MNT: Unpin pyparsing, xfail error message tests for pyparsing
 3.1.0

May require backporting a portion of #26198 to the 3.7 branch if we wish to have a 3.7.3.
(specifically the part where token, placeable, and auto_delim are excluded from setName)
---
 environment.yml                       |  2 +-
 lib/matplotlib/tests/test_mathtext.py |  8 ++++++++
 lib/matplotlib/tests/test_text.py     | 10 ++++++++++
 setup.py                              |  2 +-
 4 files changed, 20 insertions(+), 2 deletions(-)

diff --git a/environment.yml b/environment.yml
index cd043239472e..c35b90e9acba 100644
--- a/environment.yml
+++ b/environment.yml
@@ -19,7 +19,7 @@ dependencies:
   - pillow>=6.2
   - pybind11>=2.6.0
   - pygobject
-  - pyparsing!=3.1.0
+  - pyparsing>=2.3.1
   - pyqt
   - python-dateutil>=2.1
   - setuptools
diff --git a/lib/matplotlib/tests/test_mathtext.py b/lib/matplotlib/tests/test_mathtext.py
index f281b1e47412..025b79aaa4ed 100644
--- a/lib/matplotlib/tests/test_mathtext.py
+++ b/lib/matplotlib/tests/test_mathtext.py
@@ -9,13 +9,18 @@
 from typing import Any
 
 import numpy as np
+from packaging.version import parse as parse_version
+import pyparsing
 import pytest
 
+
 import matplotlib as mpl
 from matplotlib.testing.decorators import check_figures_equal, image_comparison
 import matplotlib.pyplot as plt
 from matplotlib import mathtext, _mathtext
 
+pyparsing_version = parse_version(pyparsing.__version__)
+
 
 # If test is removed, use None as placeholder
 math_tests = [
@@ -279,6 +284,9 @@ def test_fontinfo():
     assert table['version'] == (1, 0)
 
 
+# See gh-26152 for more context on this xfail
+@pytest.mark.xfail(pyparsing_version.release == (3, 1, 0),
+                   reason="Error messages are incorrect for this version")
 @pytest.mark.parametrize(
     'math, msg',
     [
diff --git a/lib/matplotlib/tests/test_text.py b/lib/matplotlib/tests/test_text.py
index bd3a88fa61a5..4374a8d9026d 100644
--- a/lib/matplotlib/tests/test_text.py
+++ b/lib/matplotlib/tests/test_text.py
@@ -4,6 +4,8 @@
 
 import numpy as np
 from numpy.testing import assert_almost_equal
+from packaging.version import parse as parse_version
+import pyparsing
 import pytest
 
 import matplotlib as mpl
@@ -16,6 +18,8 @@
 from matplotlib.testing._markers import needs_usetex
 from matplotlib.text import Text, Annotation
 
+pyparsing_version = parse_version(pyparsing.__version__)
+
 
 @image_comparison(['font_styles'])
 def test_font_styles():
@@ -818,6 +822,9 @@ def test_unsupported_script(recwarn):
          (r"Matplotlib currently does not support Bengali natively.",)])
 
 
+# See gh-26152 for more information on this xfail
+@pytest.mark.xfail(pyparsing_version.release == (3, 1, 0),
+                   reason="Error messages are incorrect with pyparsing 3.1.0")
 def test_parse_math():
     fig, ax = plt.subplots()
     ax.text(0, 0, r"$ \wrong{math} $", parse_math=False)
@@ -828,6 +835,9 @@ def test_parse_math():
         fig.canvas.draw()
 
 
+# See gh-26152 for more information on this xfail
+@pytest.mark.xfail(pyparsing_version.release == (3, 1, 0),
+                   reason="Error messages are incorrect with pyparsing 3.1.0")
 def test_parse_math_rcparams():
     # Default is True
     fig, ax = plt.subplots()
diff --git a/setup.py b/setup.py
index f58b30536aa3..de8976cb70d2 100644
--- a/setup.py
+++ b/setup.py
@@ -335,7 +335,7 @@ def make_release_tree(self, base_dir, files):
         "numpy>=1.21",
         "packaging>=20.0",
         "pillow>=6.2.0",
-        "pyparsing>=2.3.1,<3.1",
+        "pyparsing>=2.3.1",
         "python-dateutil>=2.7",
     ] + (
         # Installing from a git checkout that is not producing a wheel.
