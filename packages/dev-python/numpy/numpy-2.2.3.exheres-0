# Copyright 2009 Sterling X. Winter <replica@exherbo.org>
# Copyright 2010 Cecil Curry <leycec@gmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'numpy-1.3.0.ebuild' from Gentoo, which is:
#     Copyright 1999-2009 Gentoo Foundation

require pypi py-pep517 [ backend=meson-python backend_version_spec="[>=0.15.0]" blacklist="3.8 3.9" entrypoints=[ f2py ${PN}-config ] test=pytest ]
require flag-o-matic

SUMMARY="Fundamental package for array computing in Python"
DESCRIPTION="
NumPy is the fundamental package needed for scientific computing with Python.
It contains:

  * a powerful N-dimensional array object
  * sophisticated broadcasting functions
  * basic linear algebra functions
  * basic Fourier transforms
  * sophisticated random number capabilities
  * tools for integrating Fortran code
  * tools for integrating C/C++ code

Besides its obvious scientific uses, NumPy can also be used as an efficient
multi-dimensional container of generic data. Arbitrary data-types can be
defined. This allows NumPy to seamlessly and speedily integrate with a wide
variety of databases.
"
HOMEPAGE+=" https://${PN}.org"

UPSTREAM_DOCUMENTATION="https://${PN}.org/doc/$(ever range 1-2)/ [[ lang = en ]]"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS=""

RESTRICT="test"

DEPENDENCIES="
    build:
        dev-python/Cython[>=3.0.6][python_abis:*(-)?]
    build+run:
        sys-libs/libgfortran:=
        virtual/blas
        virtual/lapack
    test:
        dev-python/hypothesis[>=5.3.0][python_abis:*(-)?]
        dev-python/pytest-timeout[python_abis:*(-)?]
"

src_prepare() {
    # FIXME: hack to allow meson-python to find gfortran
    local dir=${WORKBASE}/symlinked-build-tools
    edo mkdir -p ${dir}
    edo ln -s /usr/host/bin/$(exhost --target)-gfortran ${dir}/gfortran
    export PATH="${dir}:${PATH}"

    py-pep517_src_prepare
}

configure_one_multibuild() {
    filter-flags -flto

    # https://github.com/numpy/numpy/issues/25004
    append-flags -fno-strict-aliasing

    py-pep517_prepare_one_multibuild
}

prepare_one_multibuild() {
    # respect selected python abi
    edo sed \
        -e "s:python3:python$(python_get_abi):g" \
        -i meson.build

    # FIXME: use meson.options -Dblas=blas & -Dlapack=lapack
    # https://gitlab.exherbo.org/exherbo/arbor/-/issues/70
    # https://gitlab.exherbo.org/exherbo/python/-/merge_requests/1040
    if [[ $(eclectic blas show) != OpenBLAS ]]; then
        edo sed \
            -e '/blas-order/s:auto:blas:' \
            -e '/lapack-order/s:auto:lapack:' \
            -i meson.options
    else
        edo sed \
            -e '/blas-order/s:auto:openblas:' \
            -e '/lapack-order/s:auto:lapack:' \
            -i meson.options
    fi

    py-pep517_prepare_one_multibuild
}

install_one_multibuild() {
    py-pep517_install_one_multibuild

    # Remove installed tests
    edo rm -rf "${IMAGE}"/$(python_get_sitedir)/${PN}/f2py/tests
}

