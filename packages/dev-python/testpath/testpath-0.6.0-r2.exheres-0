# Copyright 2018 Alexander Kapshuna <kapsh@kap.sh>
# Copyright 2024 Ali Polatel <alip@chesswob.org>
# Distributed under the terms of the GNU General Public License v2

require pypi py-pep517 [ backend=flit_core ]

SUMMARY="Testing utilities which use and manipulates filesystem and system commands"
HOMEPAGE="https://testpath.readthedocs.io/"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    suggestion:
        dev-python/pytest[python_abis:*(-)?] [[
            description = [ required for the test suite to run ]
        ]]
"

test_one_multibuild() {
    # avoid a setuptools_scm, pytest dependency loop
    if has_version dev-python/pytest[python_abis:$(python_get_abi)] ; then
        esandbox allow_net "unix:${TEMP%/}/tmp*/asocket"
        py-pep517_run_tests_pytest
        esandbox disallow_net "unix:${TEMP%/}/tmp*/asocket"
    else
        ewarn "dev-python/pytest[python_abis:$(python_get_abi)] not yet installed, skipping tests."
    fi
}

