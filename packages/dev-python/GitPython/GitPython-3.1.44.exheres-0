# Copyright 2015 Benedikt Morbach <moben@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require py-pep517 [ backend=setuptools test=pytest ]
# The tests use the git repo as reference data, so we use scm-git even for releases
# NOTE(moben): maybe extend github.exlib to allow using git for releases as well, but so far only
# gitdb and GitPython need it
SCM_BRANCH="main"
SCM_TAG=${PV}
SCM_REPOSITORY="https://github.com/gitpython-developers/${PN}"
SCM_gitdb_REPOSITORY="https://github.com/gitpython-developers/gitdb"
SCM_smmap_REPOSITORY="https://github.com/gitpython-developers/smmap"

SCM_SECONDARY_REPOSITORIES="gitdb smmap"
SCM_EXTERNAL_REFS="git/ext/gitdb:gitdb"
SCM_gitdb_EXTERNAL_REFS="gitdb/ext/smmap:smmap"

require scm-git

SUMMARY="A python library used to interact with Git repositories"
DESCRIPTION="
It provides abstractions of git objects for easy access of repository data, and additionally allows
you to access the git repository more directly using either a pure python implementation, or the
faster, but more resource intensive git command implementation."
HOMEPAGE="https://github.com/gitpython-developers/${PN}"
REMOTE_IDS="github:gitpython-developers/${PN}"
UPSTREAM_DOCUMENTATION="http://gitpython.readthedocs.org/"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/future[>=0.9]
        dev-python/gitdb[>=4.0.1&<5][python_abis:*(-)?]
        dev-scm/git
    test:
        dev-python/ddt[>=1.1.1][python_abis:*(-)?]
        dev-python/pytest-cov[python_abis:*(-)?]
        dev-python/pytest-mock[python_abis:*(-)?]
"

# After running tests sydbox freezes while (or after?) trying to disallow $TEMP
RESTRICT="test"  # last checked: 3.1.30

PYTEST_SKIP=(
    # Both tests want to clone a repo from gitlab.com
    test_installation
    test_leaking_password_in_clone_logs
)

prepare_one_multibuild() {
    # tests expect HEAD == master, set master to the commit we want, so everyone is happy
    edo git checkout -B master ${PV}

    # tests need a working git setup
    edo git config --global user.name "paludis"
    edo git config --global user.email "paludis@example.com"

    # tests need a non-empty reflog https://github.com/gitpython-developers/GitPython/issues/286
    edo git commit --allow-empty --message "Empty commit to please tests"

    # test_git_submodules_and_add_sm_with_new_commit and
    # test_list_only_valid_submodules fail without this
    edo git config --global protocol.file.allow "always"

    # pytest-sugar is unwritten
    edo sed -e "s/--force-sugar //" -i pyproject.toml

    py-pep517_prepare_one_multibuild
}

test_one_multibuild() {
    local port=$((RANDOM+1024))
    esandbox allow_net "inet:127.0.0.1@${port}"
    esandbox allow_net --connect "inet:127.0.0.1@${port}"

    export GIT_PYTHON_TEST_GIT_DAEMON_PORT=${port}
    export TMPDIR=${TEMP}/GitPython-testdir$(python_get_abi)
    edo mkdir "${TMPDIR}"

    export LC_ALL=en_GB.utf8
    py-pep517_test_one_multibuild
    unset LC_ALL

    unset GIT_PYTHON_TEST_GIT_DAEMON_PORT
    TMPDIR=${TEMP}

    esandbox disallow_net --connect "inet:127.0.0.1@${port}"
    esandbox disallow_net "inet:127.0.0.1@${port}"
}

