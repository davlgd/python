# Copyright 2015 Volodymyr Medvid <vmedvid@riseup.net>
# Distributed under the terms of the GNU General Public License v2

require github [ user=mitsuhiko ]
require setup-py [ import=setuptools test=pytest ] pypi
require utf8-locale

SUMMARY="A simple wrapper around optparse for powerful command line utilities"
DESCRIPTION="
Click is a Python package for creating beautiful command line interfaces
in a composable way with as little code as necessary. It's the 'Command
Line Interface Creation Kit'. It's highly configurable but comes with
sensible defaults out of the box.
"

UPSTREAM_DOCUMENTATION="https://${PN}.palletsprojects.com/"
UPSTREAM_RELEASE_NOTES="${UPSTREAM_DOCUMENTATION}en/$(ever major).x/changelog/#version-${PV/./-}"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="doc [[ description = [ Install HTML documentation ] ]]"

DEPENDENCIES="
    build:
        doc? (
            dev-python/Sphinx[>=2.4.4]
            dev-python/sphinx-issues[>=1.2.0]
            dev-python/sphinxcontrib-log-cabinet[>=1.0.1]
            dev-python/Pallets-Sphinx-Themes[>=1.2.3]
        )
"

DEFAULT_SRC_INSTALL_EXCLUDE=( PKG-INFO )

pkg_setup() {
    # required for tests
    require_utf8_locale
}

compile_one_multibuild() {
    SETUP_PY_SRC_COMPILE_PARAMS=(
        $(option doc build_sphinx)
    )
    setup-py_compile_one_multibuild
}

install_one_multibuild() {
    setup-py_install_one_multibuild

    # Install HTML docs excluding intersphinx file
    if option doc; then
        edo pushd build/sphinx
        edo rm html/objects.inv
        dodoc -r html
        edo popd
    fi
}
