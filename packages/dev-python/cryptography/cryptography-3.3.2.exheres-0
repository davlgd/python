# Copyright 2014 Calvin Walton <calvin.walton@kepstin.ca>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools test=pytest ]

SUMMARY="Cryptographic recipes and primitives for Python developers"
DESCRIPTION="
cryptography is a Python library which exposes cryptographic recipes and
primitives. Our goal is for it to be your 'cryptographic standard library'.

cryptography includes both high level recipes, and low level interfaces
to common cryptographic algorithms such as symmetric ciphers, message
digests and key derivation functions.
"
HOMEPAGE+=" https://cryptography.io"

LICENCES="|| ( Apache-2.0 BSD-3 )"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        dev-python/cffi[>=1.12][python_abis:*(-)?]
        dev-python/six[>=1.4.1][python_abis:*(-)?]
        providers:libressl? ( dev-libs/libressl:=[>=2.9.1] )
        providers:openssl? ( dev-libs/openssl:=[>=1.1.0] )
        python_abis:2.7? (
            dev-python/enum34[python_abis:2.7]
            dev-python/ipaddress[python_abis:2.7]
        )
    test:
        dev-python/cryptography-vectors[~${PV}][python_abis:*(-)?]
        dev-python/hypothesis[>=1.11.4][python_abis:*(-)?]
        dev-python/iso8601[python_abis:*(-)?]
        dev-python/pretend[python_abis:*(-)?]
        dev-python/pytest[>=3.6.0][python_abis:*(-)?]
        dev-python/pytz[python_abis:*(-)?]
        python_abis:2.7? (
            dev-lang/python:2.7[sqlite]
        )
"

test_one_multibuild() {
    local builddir=$(ls -d build/lib.*-$(python_get_abi)/)

    # symlink egg-info into PYTHONPATH where the pkg_resources module can pick
    # it up; tests use the information from entry_points.txt to determine which
    # backends are installed
    edo ln -s "${PWD}"/src/${PN}.egg-info ${builddir}

    setup-py_test_one_multibuild

    # remove egg-info again so it doesn't get installed
    edo rm ${builddir}/${PN}.egg-info
}

