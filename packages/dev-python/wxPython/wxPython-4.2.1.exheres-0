# Copyright 2012-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools blacklist=2 test=pytest ]
require wxwidgets [ min_version='3.2.0' wxwidgets_opts='[tiff]' ]

SUMMARY="wxPython is a wxGTK-based GUI toolkit for Python"
HOMEPAGE="https://www.wxpython.org"

LICENCES="LGPL-2"
SLOT="4.2"
PLATFORMS="~amd64"
MYOPTIONS="opengl"

# Some tests are broken, last checked: 4.0.6
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-python/numpy[python_abis:*(-)?]
        dev-python/Pillow[python_abis:*(-)?]
        dev-python/sip[>=6.8.5][python_abis:*(-)?]
        dev-python/six[python_abis:*(-)?]
        opengl? ( dev-python/PyOpenGL[python_abis:*(-)?] )
    test:
        dev-python/appdirs[python_abis:*(-)?]
        dev-python/numpy[python_abis:*(-)?]
        dev-python/Pillow[python_abis:*(-)?]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/aeb557d01e7cd37176ebbf0f1ae6d0b53c115378.patch
    "${FILES}"/01787f43a5b3d996158179997046b1b7f67af15c.patch
    "${FILES}"/2c6307142f408bdc7ab9b0ba2951ba34412cb5d8.patch
    "${FILES}"/fd8781c97a8a74c6e4bc6a91a25b40f0a8f2728d.patch
)

SETUP_PY_SRC_INSTALL_PARAMS=(
    --skip-build
)

pkg_setup() {
    wxwidgets_pkg_setup

    # Re-export WX_CONFIG_NAME (exported by wxwidgets_pkg_setup) as WX_CONFIG which is used by
    # Phoenix's build.py
    export WX_CONFIG=${WX_CONFIG_NAME}
}

prepare_one_multibuild() {
    # Use prefixed pkg-config
    edo sed \
         -e "s:pkg-config :$(exhost --tool-prefix)& :" \
         -i buildtools/config.py \
         -i wscript

    setup-py_prepare_one_multibuild
}

compile_one_multibuild() {
    edo ${PYTHON} build.py sip
    # Using the build.py script seems to be the easiest way to use system wxGTK instead of building
    # and deploying the bundled version
    edo ${PYTHON} build.py build \
        --verbose \
        --release \
        --gtk3 \
        --no_magic \
        --use_syswx
}

src_compile() {
    export PKG_CONFIG=$(type -P ${PKG_CONFIG})

    setup-py_src_compile
}

