# Copyright 2024 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN=${PN/-/_}

require pypi [ pn=${MY_PN} pnv=${MY_PN}-${PV} ]
require py-pep517 [ backend=setuptools test=pytest work=${MY_PN}-${PV} ]

SUMMARY="Module for text manipulation"
DESCRIPTION="This package provides handy routines for dealing with text, such
as wrapping, substitution, trimming, stripping, prefix and suffix removal,
line continuation, indentation, comment processing, identifier processing,
values parsing, case insensitive comparison, and more."

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-python/setuptools_scm:0[>=3.4.1][python_abis:*(-)?]
    build+run:
        dev-python/autocommand[python_abis:*(-)?]
        dev-python/inflect[python_abis:*(-)?]
        dev-python/jaraco-context[>=4.1][python_abis:*(-)?]
        dev-python/jaraco-functools[python_abis:*(-)?]
        dev-python/more-itertools[python_abis:*(-)?]
        python_abis:3.8? ( dev-python/importlib_resources:0[python_abis:3.8] )
"

test_one_multibuild() {
    PYTHONPATH="$(ls -d ${PWD}/build/lib*)" edo ${PYTHON} -m pytest -k 'not jaraco.text.show-newlines.report_newlines'
}

