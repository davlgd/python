# Copyright 2010 Paul Seidler
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools blacklist=2 test=pytest ]

SUMMARY="Provide python bindings for libcurl"
DESCRIPTION="
PycURL is a Python interface to libcurl. PycURL can be used to fetch objects identified by a URL
from a Python program, similar to the urllib Python module. PycURL is mature, very fast, and
supports a lot of features.
"
HOMEPAGE+=" http://${PN}.io"

UPSTREAM_CHANGELOG="${HOMEPAGE}/docs/${PV}/ChangeLog [[ lang = en ]]"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/docs/${PV}/index.html [[ lang = en ]]"

LICENCES="|| ( LGPL-2.1 MIT )"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( providers: gnutls libressl mbedtls openssl ) [[ number-selected = exactly-one ]]
"

# NOTE: It would also work with nss if our curl had an nss option.
DEPENDENCIES="
    build+run:
        net-misc/curl[>=7.19.0][providers:*(?)=]
    test:
        dev-python/bottle[python_abis:*(-)?]
        dev-python/pyflakes[python_abis:*(-)?]
        providers:gnutls? ( dev-libs/gnutls )
        providers:libressl? ( dev-libs/libressl:= )
        providers:mbedtls? ( dev-libs/mbedtls )
        providers:openssl? ( dev-libs/openssl:= )
"

# Multiple tests fail
RESTRICT="test"

compile_one_multibuild() {
    # These tests fail
    edo rm -r examples/tests/

    SETUP_PY_SRC_COMPILE_PARAMS=(
        $(option providers:gnutls --with-gnutls)
        $(option providers:libressl --with-openssl)
        $(option providers:mbedtls --with-mbedtls)
        $(option providers:openssl --with-openssl)
    )

    setup-py_compile_one_multibuild
}

