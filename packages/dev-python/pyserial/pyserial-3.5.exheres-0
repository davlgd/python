# Copyright 2008, 2009 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'pyserial-2.2.ebuild' from Gentoo, which is:
#   Copyright 1999-2007 Gentoo Foundation

require pypi setup-py [ import=setuptools has_bin=true test=pytest ]

SUMMARY="Python Serial Port Extension"
DESCRIPTION="
This module encapsulates the access for the serial port. It provides backends
for Python running on Windows, Linux, BSD (possibly any POSIX compliant system),
Jython and IronPython (.NET and Mono). The module named 'serial' automatically
selects the appropriate backend.
Features include:
* Same class based interface on all supported platforms.
* Access to the port settings through Python 2.2+ properties.
* Port numbering starts at zero, no need to know the port name in the user
  program.
* Port string (device name) can be specified if access through numbering is
  inappropriate.
* Support for different bytesizes, stopbits, parity and flow control with
  RTS/CTS and/or Xon/Xoff.
* Working with or without receive timeout.
* File like API with 'read' and 'write' ('readline' etc. also supported)
* The files in this package are 100% pure Python. They depend on non standard
  but common packages on Windows (pywin32) and Jython (JavaComm). POSIX (Linux,
  BSD) uses only modules from the standard Python distribution).
* The port is set up for binary transmission. No NULL byte stripping, CR-LF
  translation etc. (which are many times enabled for POSIX.) This makes this
  module universally useful.
"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES=""

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( CHANGES.rst )

