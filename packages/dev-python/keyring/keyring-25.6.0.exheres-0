# Copyright 2013 Jakob Nixdorf <flocke@shadowice.org>
# Distributed under the terms of the GNU General Public License v2

require pypi
require py-pep517 [ backend=setuptools blacklist="3.8" test=pytest entrypoints=[ ${PN} ] ]
require test-dbus-daemon

SUMMARY="Store and access your passwords safely"
DESCRIPTION="
Provides easy access to safe password storage services from python applications.
Supports the KDE Wallet, GNOME Keyring and the newer Secret Service API as backends
as well as the native CryptFile and UncryptFile storage.
"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-python/setuptools_scm:0[>=3.4.1][python_abis:*(-)?]
    build+run:
        dev-python/jaraco-classes[python_abis:*(-)?]
        dev-python/jaraco-context[python_abis:*(-)?]
        dev-python/jaraco-functools[python_abis:*(-)?]
        dev-python/jeepney[>=0.4.2][python_abis:*(-)?]
        dev-python/secretstorage[>=3.2][python_abis:*(-)?]
        python_abis:3.9? ( dev-python/importlib_metadata[>=4.11.4][python_abis:3.9] )
        python_abis:3.10? ( dev-python/importlib_metadata[>=4.11.4][python_abis:3.10] )
        python_abis:3.11? ( dev-python/importlib_metadata[>=4.11.4][python_abis:3.11] )
    test:
        dev-python/pytest[>=6][python_abis:*(-)?]
        dev-python/pytest-mypy-plugins[python_abis:*(-)?]
    suggestion:
        dev-python/dbus-python[python_abis:*(-)?] [[
            description = [ Used to store passwords in the KDE Wallet ]
        ]]
"

# Needs a running Secret Service implementation
PYTEST_SKIP=( TestLibSecret )

