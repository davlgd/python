# Copyright 2016 Hong Hao <oahong@oahong.me>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools has_bin=true test=pytest ]

SUMMARY="Clean single-source support for Python 3 and 2"
DESCRIPTION="future is the missing compatibility layer between Python 2 and Python 3. It allows you
to use a single, clean Python 3.x-compatible codebase to support both Python 2 and Python 3 with
minimal overhead.
"
HOMEPAGE+=" https://python-future.org"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    test:
        dev-python/requests[python_abis:*(-)?]
"

PYTEST_PARAMS=(
    # * testHTTPConnectionSourceAddress: binds to 0.0.0.0
    # * test_remove_hooks_then_requests, test_requests_cm: want to access google.com
    # * test_moves_urllib_request_http, test_urllib_request_http: want to access
    #   http://python-future.org
    # * test_main, test_ftp: Unsure, but they error with "AssertionError: 'image/gif' != None"
    # * test_isinstance_recursion_limit, test_subclass_recursion_limi: Fail with python 3.12
    #   cf. https://github.com/PythonCharmers/python-future/issues/593
    #       https://github.com/PythonCharmers/python-future/pull/635
    -k "not testHTTPConnectionSourceAddress and not test_remove_hooks_then_requests \
        and not test_requests_cm and not test_urllib_request_http \
        and not test_moves_urllib_request_http \
        and not test_ftp and not test_main \
        and not test_isinstance_recursion_limit \
        and not test_subclass_recursion_limit"
)

